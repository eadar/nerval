import chalk from "chalk";
import moment from "moment";
import puppeteer from "puppeteer";
import hed from "html-entities-decoder";

export default class Sniffer {
  static startShedule(url) {
    return new Promise(async (resolve, reject) => {
      // Get start timing
      const startTime = moment();
      // Check if a loop is already pending
      if (Sniffer.loop) {
        console.warn(
          chalk.yellow(startTime.format("DD/MM/YYYY HH:mm:ss")),
          "A shedule has already been initiated"
        );
        return;
      }

      /**
       * Display the date of the next session in log
       */
      const displayNextSession = () =>
        console.info(
          chalk.grey(moment().format("DD/MM/YYYY HH:mm:ss")),
          "Next session at",
          moment(startTime)
            .add(1, "h")
            .format("DD/MM/YYYY HH:mm:ss")
        );

      // Set the loop
      Sniffer.loop = setInterval(async () => {
        if (Sniffer.pending) {
          console.info(
            chalk.grey(moment().format("DD/MM/YYYY HH:mm:ss")),
            "Skip sheduled session"
          );
        } else {
          displayNextSession();
          try {
            await Sniffer.start(url);
          } catch (e) {
            Sniffer.stopShedule();
          }
        }
      }, 1000 * 60 * 60);

      console.info(
        chalk.magenta(startTime.format("DD/MM/YYYY HH:mm:ss")),
        "A shedule has been started"
      );

      displayNextSession();
      try {
        await Sniffer.start(url);
        resolve();
      } catch (e) {
        Sniffer.stopShedule();
        reject(e);
      }
    });
  }
  static stopShedule() {
    clearInterval(Sniffer.loop);
    Sniffer.loop = null;
    console.info(
      chalk.magenta(moment().format("DD/MM/YYYY HH:mm:ss")),
      "A shedule has been stopped"
    );
  }
  /**
   * Start session
   * @param url
   * @returns {Promise<any>}
   */
  static start(url) {
    Sniffer.pending = true;
    return new Promise(async (resolve, reject) => {
      const startTime = moment();
      console.info(
        chalk.blue(startTime.format("DD/MM/YYYY HH:mm:ss")),
        "Start session"
      );
      try {
        // Import controllers
        const Movie = await import("../controllers/movie");
        // Init browser
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        // Go to target
        await page.goto(url);
        // Parse and filter raw
        const raw = await page.evaluate(async () => {
          return (
            [].slice
              .call(document.querySelectorAll("table td a"))
              .map(x => ({
                href: x.href,
                rawTitle: x.innerHTML
              }))
              // 1080p
              .filter(x => new RegExp("^(.*)(1080p)(.*)$", "mi").test(x.href))
              // Bluray or Webrip
              .filter(x =>
                new RegExp("^(.*)(BluRay|WEBRIP)(.*)$", "mi").test(x.href)
              )
              // French or multi
              .filter(x =>
                new RegExp("^(.*)(TRUEFRENCH|FRENCH|MULTI)(.*)$", "mi").test(
                  x.href
                )
              )
              // Has a year
              .filter(x =>
                new RegExp("^.*((19|20)\\d{2})(\\/)?$", "m").test(x.href)
              )
          );
        });
        let newMovies = raw.length;
        // Register in database
        for (const movie of raw) {
          // Extract year
          const year = movie.rawTitle.match(
            new RegExp("^.*((19|20)\\d{2})(\\/)?$", "m")
          );
          // Rebuild title
          const title = movie.rawTitle.match(
            new RegExp("^(.*)((\\sTRUEFRENCH|\\sFRENCH|\\sMULTI).*)$")
          );
          const payload = {
            title: `${hed(title[1].trim())} (${year[1]})`,
            link: movie.href,
            state: "pending",
            progress: 0
          };
          // table.push([payload.title, payload.link]);
          try {
            await Movie.default.create(payload);
          } catch (e) {
            if (e.name !== "DuplicatePrimaryKeyError") {
              reject(e);
            } else {
              newMovies--;
            }
          }
        }
        // Close browser
        await browser.close();
        // CLose session
        const endTime = moment();
        console.info(
          chalk.blue(endTime.format("DD/MM/YYYY HH:mm:ss")),
          `Close session [${moment
            .duration(endTime.diff(startTime))
            .asSeconds()}s] - ${
            newMovies
              ? newMovies > 1
                ? `${newMovies} new movies`
                : "One new movie"
              : "No new movies"
          }`
        );
        resolve();
      } catch (e) {
        const endTime = moment();
        console.warn(
          chalk.red(moment().format("DD/MM/YYYY HH:mm:ss")),
          `Close session with error [${moment
            .duration(endTime.diff(startTime))
            .asSeconds()}s]`
        );
        console.error(e);
        reject(e);
      } finally {
        Sniffer.pending = false;
      }
    });
  }
}
