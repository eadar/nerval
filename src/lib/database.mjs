import thinky from "thinky";

export default class Database {
  /**
   * Connexion
   * @param prms
   * @returns {Promise<Database.instance>}
   */
  static async connect(prms) {
    return new Promise(async (resolve, reject) => {
      try {
        Database.instance = await thinky(prms);
        resolve(Database.instance);
      } catch (e) {
        reject(e);
      }
    });
  }
}
