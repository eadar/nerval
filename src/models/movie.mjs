import Database from "../lib/database";

export default Database.instance.createModel("Movie", {
  id: Database.instance.type.string(),
  title: Database.instance.type.string(),
  link: Database.instance.type.string(),
  state: Database.instance.type.string(),
  progress: Database.instance.type.number()
}, {
	pk: "title"
});
