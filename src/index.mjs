import Database from "./lib/database";
import Sniffer from "./lib/sniffer";

(async () => {
  try {
    // Connect to database
    await Database.connect({ host: "195.154.63.136", db: "rss" });

    // Start sniffing session
    await Sniffer.startShedule(
      "https://wvw.torrent9.uno/torrents_films_1080p.html"
    );
  } catch (err) {
    console.error(err);
  }
})();
