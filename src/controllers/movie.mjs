import Movie from "../models/movie";

export default class MovieController {
  /**
   * Create a new movie
   * @returns {*}
   */
  static create(payload) {
    const movie = new Movie(payload);
    return movie.save();
  }

  /**
   * Create a new movie
   * @returns {*}
   */
  static retreive(payload) {
    return Movie.filter(payload);
  }
}
