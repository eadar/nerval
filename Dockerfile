FROM node:latest

WORKDIR /usr/src/app

COPY . .

RUN yarn

CMD node -r @std/esm src/index.mjs
